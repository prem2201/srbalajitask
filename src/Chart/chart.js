import { Grid, Box } from '@mui/material';
import React from 'react';
import {
    PieChart, Pie, Cell, Legend, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip,
    BarChart, Bar,
} from 'recharts';
import { chartStyle } from './style';
const data = [
    { name: 'Group A', value: 1 },
    { name: 'Group B', value: 2 },
    { name: 'Group C', value: 11 }
];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const linedata = [
    {
        name: 'A',
        uv: 4000,
        pv: 2400,
        amt: 2400,
    },
    {
        name: 'B',
        uv: 3000,
        pv: 1398,
        amt: 2210,
    },
    {
        name: 'C',
        uv: 2000,
        pv: 9800,
        amt: 2290,
    },
    {
        name: 'D',
        uv: 2780,
        pv: 3908,
        amt: 2000,
    },
    {
        name: 'E',
        uv: 1890,
        pv: 4800,
        amt: 2181,
    },
    {
        name: 'F',
        uv: 2390,
        pv: 3800,
        amt: 2500,
    },
    {
        name: 'G',
        uv: 3490,
        pv: 4300,
        amt: 2100,
    },
];

const bardata = [
    {
        name: "A",
        one: 400,
        two: 240,
        three: 300,
        amt: 2400
    },
    {
        name: "B",
        one: 300,
        amt: 2210
    },
    {
        name: "C",
        one: 2000,
        two: 980,
        amt: 2290
    },
    {
        name: "D",
        one: 278,
        two: 390,
        amt: 2000
    },
    {
        name: "E",
        one: 189,
        two: 480,
        amt: 2181
    }
];

export const Chart = () => {
    const classes = chartStyle()
    return (
        <Box>
            <Grid container>
                <Grid item  xl={4} md={6}>
                    <Box className={classes.chartCard}>
                        <PieChart width={400} height={400}>
                            <Pie
                                data={data}
                                cx={200}
                                cy={200}
                                labelLine={false}

                                outerRadius={80}
                                fill="#8884d8"
                                dataKey="value"
                            >
                                {data.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                ))}
                            </Pie>
                            <Legend iconType={"circle"} iconSize={8} />
                        </PieChart>
                    </Box>
                </Grid>
                <Grid item  xl={8} md={6}>
                    <Box className={classes.chartCard}>
                        <LineChart
                            width={600}
                            height={300}
                            data={linedata}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{ r: 8 }} />
                            <Line type="monotone" dataKey="uv" stroke="#82ca9d" />
                        </LineChart>
                    </Box>
                </Grid>
                <Grid item  xl={4} md={12}>
                    <BarChart
                        width={500}
                        height={300}
                        data={bardata}
                        margin={{
                            top: 20,
                            right: 30,
                            left: 20,
                            bottom: 5
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend iconType={"circle"} iconSize={8} />
                        <Bar dataKey="one" stackId="a" fill="#8884d8" />
                        <Bar dataKey="two" stackId="a" fill="#82ca9d" />
                        <Bar dataKey="three" stackId="a" fill="#000" /> !==
                    </BarChart>
                </Grid>
            </Grid>
        </Box>
    )
}