import { makeStyles } from '@mui/styles';

export const chartStyle = makeStyles({
    chartCard:{
        height:"100%",
        width:"100%",
        borderRadius:5,
        border:"1px solid #AAA"
    }
})