import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import NotFound from './NotFound';
import { HomeComp } from './HomeComp';
import { ProfileComp } from './CRED/Profile';
import { ChartComp } from './ChartComp';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
      <Route exact path="/" element={<HomeComp />} />
        <Route exact path="/profile" element={<ProfileComp/>}/>
        <Route exact path="/chart" element={<ChartComp/>}/>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>

  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
