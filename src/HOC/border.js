import React from "react";

export const EnhanceBorder = originalComponent =>{
    function NewComponent(){
        return(
            <div style={{border:"1px solid black"}}>
                <originalComponent/>
            </div>
        )
    }
    return NewComponent
}