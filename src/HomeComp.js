import axios from "axios";
import React from "react";
import { ModelComp } from "./CRED/ModelComp";
import { TableComp } from "./CRED/TableComp";


export const Context = React.createContext()
const initialState = {
    name: "",
    email_id: "",
    phone: "",
    url: "",
    error: {
        name: "",
        email_id: "",
        phone: "",
    }
}
export const HomeComp = () => {
    const [data, setData] = React.useState({ ...initialState })
    const [list, setList] = React.useState([{}])
    const [open, setOpen] = React.useState(false);
    const [edit, setEdit] = React.useState({
        bool: false,
        editedIndex: null
    })

    const updateState = (key, value) => {
        setData({ ...data, [key]: value })
    }

    //Base URL for API
    const baseURL = "https://62d65b9551e6e8f06f0777f9.mockapi.io/api/v1"
    // Get data from API
    const getData = () => {
        axios.get(`${baseURL}/crud`).then((res) => {
            setList(res?.data)
        })
    }

    React.useEffect(() => {
        getData()
    }, [])

    const handleOpen = () => {
        setOpen(true)
        setEdit({
            bool: false,
            editedIndex: null
        })
    };
    const handleClose = () => setOpen(false);

    // Submit form values
    const submit = (value) => {
        if (validateForm()) {
            axios.post(`${baseURL}/crud`, value).then((res) => {
                getData()
                // Clear form value
                setData(initialState)
                // Change edit status
                setEdit({
                    bool: false,
                    editedIndex: null
                })
                // Close model
                setOpen(false)
            })
        } else {
            return false
        }
    }
    // Close Model
    const closeModel = () => {
        setOpen(false)
    }
    //validate form
    const validateForm = () => {
        let isValid = true;
        let error = data.error;
        let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //validate name
        if (data?.name?.length === 0) {
            isValid = false;
            error.name = "Name is Required";
        }
        //validate email
        if (data?.email_id?.length === 0) {
            isValid = false;
            error.email_id = "Email is Required";
        }
        //validate correct email
        if (data?.email_id?.length > 0) {
            if (Boolean(re.test(data?.email_id)) === false) {
                isValid = false;
                error.email_id = "Invalid Email";
            }
        }
        //validate phone
        if (data?.phone?.length === 0) {
            isValid = false;
            error.phone = "Phone is Required";
        }
        //validate correct phone
        if (data?.phone?.length > 0) {
            if (data?.phone?.length !== 10) {
                isValid = false;
                error.phone = "Phone Number Must be 10";
            }
        }
        setData({ ...data, error });
        return isValid;
    }

    const editRowData = (i, row) => {
        setData(row)
        setOpen(true)
        setEdit({
            bool: true,
            editedIndex: i
        })
    }

    const deleteRowData = (i, row) => {
        axios.delete(`${baseURL}/crud/${row.id}`).then(() => {
            getData()
        })
    }

    // Edit old Record
    const onEdit = (data) => {
        axios.put(`${baseURL}/crud/${data.id}`, data).then((res) => {
            getData()
        })

        setOpen(false)
        setData(initialState)
        setEdit({
            bool: false
        })
    }


    return (
        <Context.Provider value={{
            list, open, data, edit, editRowData, deleteRowData,
            onEdit, updateState, submit, setOpen, handleOpen, handleClose, closeModel
        }}>
            <div className="container">
                <ModelComp />
                <TableComp />
            </div>
        </Context.Provider>

    )
}