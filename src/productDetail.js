import './Home.css'
import { Link, useLocation, useSearchParams } from 'react-router-dom'
import { useEffect, useState } from 'react';
import axios from 'axios';
// For card
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Button } from '@mui/material';
import { Grid } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";
import Rating from "@material-ui/lab/Rating";
// End Card
import Alert from '@mui/material/Alert';
export default function ProductDetail() {
    const [searchParams] = useSearchParams();
    const [productDetail, setproductDetail] = useState()
    const [loading, setLoading] = useState(true)

    // axios.get('https://fakestoreapi.com/products/' + searchParams.get('id')).then(res => {
    //     setproductDetail(res.data)
    //     setLoading(false)
    // })
    const Btnclick = () =>{
        return <Alert severity="success">This is a success alert — check it out!</Alert>
    }
    return (
        <div>

            {/* {loading ? <div className="loader"></div> :
                <div>
                    <Link to="/home">Home</Link>
                    <Card>
                        <CardHeader
                            avatar={
                                <Avatar sx={{ bgcolor: red[500] }} aria-label="product">
                                    R
                                </Avatar>
                            }
                            action={
                                <IconButton aria-label="settings">
                                    <MoreVertIcon />
                                </IconButton>
                            }

                            title={productDetail?.title}
                            subheader={productDetail?.category}
                        />
                        <Rating
                            name="half-rating-read"
                            value={productDetail.rating.rate}
                        />
                        <CardMedia
                            component="img"
                            image={productDetail?.image}
                            alt="Paella dish"
                        />
                        <CardContent>
                            <Typography>
                                {productDetail?.description}
                            </Typography>
                        </CardContent>
                        <CardActions disableSpacing>
                            <IconButton aria-label="add to favorites">
                                <FavoriteIcon />
                            </IconButton>
                            <IconButton aria-label="share">
                                <ShareIcon />
                            </IconButton>
                        </CardActions>
                    </Card>
                </div>} */}

                <button onClick={Btnclick}>Alert</button>
                {/* <Btnclick /> */}
        </div>
    )
}