import * as React from 'react';
import {
    Table, TableBody, TableCell, tableCellClasses,
    TableContainer, TableHead, TableRow, IconButton, Avatar
} from '@mui/material';
import { useNavigate } from "react-router-dom";
import Paper from '@mui/material/Paper';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { CREDStyle } from './CREDStyle';
import { styled } from '@mui/material/styles';
import { Context } from '../HomeComp';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
        padding: 8
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

export const TableComp = () => {
    //Context
    const {editRowData,deleteRowData,list} = React.useContext(Context)
    //Classes
    const tableStyle = CREDStyle()
    //navigation
    const navigate = useNavigate()
    //Profile navigate 
    const navigateProfile = (row) => { navigate("/profile", { state: { row } }) }
    return (
        <>
            <TableContainer component={Paper} className={tableStyle.table}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Profile</StyledTableCell>
                            <StyledTableCell>Name</StyledTableCell>
                            <StyledTableCell align="center">Email</StyledTableCell>
                            <StyledTableCell align="center">Phone No</StyledTableCell>
                            <StyledTableCell align="center">Edit</StyledTableCell>
                            <StyledTableCell align="center">Delete</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {list?.map((row, index) => (
                            
                            <StyledTableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <StyledTableCell align="center">                                    
                                    <Avatar onClick={() => { navigateProfile(row) }} alt="" src={row.avatar} />
                                </StyledTableCell>
                                <StyledTableCell component="th" scope="row">
                                    {row.name}
                                </StyledTableCell>
                                <StyledTableCell align="center">{row.email_id}</StyledTableCell>
                                <StyledTableCell align="center">{row.phone}</StyledTableCell>
                                <StyledTableCell align='center'>
                                    <IconButton color="primary" aria-label="Edit">
                                        <EditIcon onClick={() => editRowData(index, row)} />
                                    </IconButton>
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    <IconButton color="primary" aria-label="Edit">
                                        <DeleteIcon onClick={() => deleteRowData(index, row)} />
                                    </IconButton>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    )
}