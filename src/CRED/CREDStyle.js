import { makeStyles } from '@mui/styles';

export const CREDStyle = makeStyles({
    model: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        height:"auto",
        background:'white',
        border: '2px solid #efefef',
        borderRadius:5,
        boxShadow: 24,
        overflow:"auto",
        p: 4,
    },
    table:{
        maxHeight:500,
        maxWidth:'auto',
        marginTop:20
    },
    addButton:{
        display:'flex',
        justifyContent:'right',
        alignItems:'right', 
        padding:5,
        marginTop:10
    },
    formlabel:{
        color:"#BDC2C9",
        fontSize:8
    },
    formHeadTitle:{
        fontSize:20,
        fontWeight:"bold"
    },
    submitBtn:{
        position:"relative",
        marginTop:30
    }
})