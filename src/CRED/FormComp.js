import CloseIcon from '@mui/icons-material/Close';
import { Box, Button, Divider, IconButton, Stack, TextField, Typography } from '@mui/material';
import React from 'react';
import { Context } from './../HomeComp';
import { CREDStyle } from './CREDStyle';

export const FormComp = () => {

    // Context
    const { data, updateState, submit, onEdit, edit, closeModel } = React.useContext(Context)
    // Classes
    const Style = CREDStyle()

    //Submit datas Add or Edit
    const submitData = (event) => {
        event.preventDefault()
        edit?.bool ? onEdit(data) : submit(data)
    }
    return (
        <Box sx={{ maxHeight: "auto" }}>
            <Stack direction={"column"}>
                <Box m={1}>
                    <Stack direction={"row"} justifyContent={"space-between"} alignItems={"center"}>
                        <h5> {edit?.bool ? "Edit" : "Add"} </h5>
                        <IconButton aria-label="close" onClick={closeModel}><CloseIcon /></IconButton>
                    </Stack>

                </Box>
                <Divider />
                <Box m={2}>
                    <Stack direction={"column"} spacing={1}>
                        <Typography className={Style.formlabel}>Name</Typography>
                        <TextField
                            helperText={data?.error?.name}
                            variant="standard"
                            size='small'
                            error={data?.error?.name?.length > 0}
                            onChange={(e) => updateState("name", e.target.value)} value={data?.name}
                        />
                        <Typography className={Style.formlabel}>Email Id</Typography>
                        <TextField
                            helperText={data?.error?.email_id}
                            error={data?.error?.email_id?.length > 0}
                            variant="standard"
                            size='small'
                            onChange={(e) => updateState("email_id", e.target.value)} value={data?.email_id}
                        />
                        <Typography className={Style.formlabel}>Phone No</Typography>
                        <TextField
                            helperText={data?.error?.phone}
                            variant="standard"
                            size='small'
                            type="number"
                            error={data?.error?.phone?.length > 0}
                            onChange={(e) => updateState("phone", e.target.value)} value={data?.phone}
                        />

                        <Box sx={{ marginTop: "40px" }}>
                            <Button sx={{ marginTop: "25px", width: "100%" }} variant="contained" p={3} onClick={submitData}>SUBMIT</Button>
                        </Box>
                    </Stack>
                </Box>
            </Stack>
        </Box>
    )
}