import React from 'react';
import { Box, Button, Modal } from '@mui/material';
import { CREDStyle } from './CREDStyle';
import { FormComp } from './FormComp';
import AddIcon from '@mui/icons-material/Add';

import {Context} from './../HomeComp'
export const ModelComp = () => {

    // Get props value from context
    const {open,handleOpen,handleClose} = React.useContext(Context)
    // classes
    const Style = CREDStyle()
    return (
        <>
            {/* <Button variant="outlined" onClick={handleOpen}>Open modal</Button> */}
            <div className={Style.addButton}>
                <Button variant="contained" startIcon={<AddIcon />} onClick={handleOpen}>
                    Add
                </Button>
            </div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className={Style.model}>
                    <FormComp />
                </Box>
            </Modal>
        </>
    )
}