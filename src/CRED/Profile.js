import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Avatar, Card, CardContent, CardHeader, CardMedia, IconButton } from '@mui/material';
import { useLocation } from 'react-router-dom';

export const ProfileComp = () => {
    const location = useLocation();
    const listData = location.state.row
    return (
        <>
            <Card sx={{ maxWidth: 350, maxHeight: 700 }}>
                <CardHeader
                    avatar={
                        <Avatar alt='' src={URL.createObjectURL(listData?.url)} />
                    }
                    action={
                        <IconButton aria-label="settings">
                            <MoreVertIcon />
                        </IconButton>
                    }
                    title="Shrimp and Chorizo Paella"
                    subheader="September 14, 2016"
                />
                <CardMedia
                    component="img"
                    height="194"
                    image={URL.createObjectURL(listData?.url)}
                    alt="Paella dish"
                />
                <CardContent>

                </CardContent>
            </Card>
        </>
    )
}